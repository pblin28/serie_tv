# TP n°6 : Séries TV

## Objectif attendu
>Réalisation de deux pages web concernant une Série TV que l'on a choisi.

# Explications

## Quelques mots d'explications

Tout d'abord, j'ai décidé d'utiliser l'outil **__flex__** car le **__main__** et le **__aside__** de la page n'avaient pas un **__comportement naturel__**. Il fallait donc que j'établisse leur parent, **__body__**, en tant que `flex-container` mais également eux-mêmes, en tant que flex-container, et qui étaient par conséquent, à la fois `flex-container` et `flex-items`. 

Suite à cela, j'ai pu disposer le main et le aside sur la page de manière à ce que leurs contenus s'affiche où je l'ai défini.

De plus, j'ai décidé d'utiliser l'outil flex pour que cela me permette de construire ma page web selon la façon que j'avais envisagée, imaginée et d'avoir plus de **__facilité__** lors du **__placement__** de mes élements (textes, images, etc..)

Par exemple, il fallait que je fasse en sorte d'avoir mes images puis mon texte correspondant aligné en dessous. Le flex m'a également permis le **__retour à la ligne__** de chacun de mes paragraphes afin d'avoir des paragraphes en fonction de quelle section je parle sur ma page.

## Travail Bonus


Dans le aside des deux pages web que j'ai construite, il y a une petite rubrique **__"En savoir plus"__** suivi d'une image sur laquelle vous pouvez cliquer puisque j'ai "imbriquer" un lien, afin d'accéder à l'actualité (parfois plus très jeune) sur la Série TV que je présente.

De plus, j'ai mis une **__vidéo__** sur ma page Accueil, qui est le générique original afin que la personne qui accède à mon site ait une petite idée de à quoi ressemble la Série TV sur laquelle j'ai basé mes pages web.

# Information supplémentaire

## Dépôt GitLab 

>Je mets à disposition le lien vers mon dépôt GitLab juste ici :
https://gitlab.com/pblin28/serie_tv

